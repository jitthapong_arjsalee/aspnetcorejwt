﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AspNetCoreJwt.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthenController : ControllerBase
    {
        private readonly IOptions<JwtAuthentication> _jwtAuthentication;

        public AuthenController(IOptions<JwtAuthentication> jwtAuthentication)
        {
            _jwtAuthentication = jwtAuthentication;
        }

        [HttpGet]
        [Route("token")]
        [AllowAnonymous]
        public ActionResult GetToken(string username="", string password="")
        {
            var jwtToken = new JwtSecurityToken(
                            issuer: _jwtAuthentication.Value.ValidIssuer,
                            audience: _jwtAuthentication.Value.ValidAudience,
                            claims: new[]{
                                new Claim(JwtRegisteredClaimNames.Sub, username),
                                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                            },
                            expires: DateTime.UtcNow.AddDays(30),
                            notBefore: DateTime.UtcNow,
                            signingCredentials: _jwtAuthentication.Value.SigningCredentials);
            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            return Ok(token);
        }

        [HttpGet]
        public ActionResult CheckToken()
        {
            return Ok("Authorized");
        }
    }
}